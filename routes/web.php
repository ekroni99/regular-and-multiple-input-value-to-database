<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home',[HomeController::class,'home'])->name('home');
Route::post('/home',[HomeController::class,'add_job'])->name('add_job');
Route::get('/view_job',[HomeController::class,'view_job'])->name('view_job');
Route::get('/all_view',[HomeController::class,'all_view'])->name('all_view');
