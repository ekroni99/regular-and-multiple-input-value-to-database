<!DOCTYPE html>
<html lang="en">

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>

<body>
    <div class="container mt-5">
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
       
        <div class="container2 w-50 border-black border p-4 rounded m-auto">
            <form action="{{ route('add_job') }}" method="POST">
                @csrf
                <div class="mb-3">
                    <label for="name" class="form-label">Name:</label>
                    <input type="text" class="form-control" id="name" name="name" required
                        placeholder="enter your name">
                </div>

                <div class="mb-3">
                    <label for="email" class="form-label">Email:</label>
                    <input type="email" class="form-control" id="email" name="email" required
                        placeholder="enter your email">
                </div>

                <div class="mb-3">
                    <label for="password" class="form-label">Password:</label>
                    <input type="password" class="form-control" id="password" name="password" required
                        placeholder="enter your password">
                </div>

                <div class="mb-3" id="detailsContainer">
                    <label for="details" class="form-label">Details:</label>
                    <input type="text" class="form-control" name="details[]" placeholder="enter your details">
                </div>

                <button type="button" class="btn btn-primary mb-0" id="addMore">Add More Details</button>

                <button type="submit" class="btn btn-success">Submit</button>
            </form>
        </div>

    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
    </script>

    <script>
        document.getElementById("addMore").addEventListener("click", function() {
            var container = document.getElementById("detailsContainer");
            // Create a line break
            var lineBreak = document.createElement("br");
            container.appendChild(lineBreak);
            // Create a new input field
            var input = document.createElement("input");
            input.type = "text";
            input.className = "form-control";
            input.name = "details[]";
            input.placeholder = "enter your details";
            container.appendChild(input);
        });
    </script>
</body>

</html>
