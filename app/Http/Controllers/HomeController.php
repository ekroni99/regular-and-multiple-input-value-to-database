<?php

namespace App\Http\Controllers;

use App\Models\FormSubmission;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(){
        return view('home');
    }
    public function add_job(Request $request){
        $details=json_encode($request->details);
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|string|max:255',
            'details.*' => 'nullable|string|max:255',
        ]);

        // $jsonData=json_encode($request->details);
        $formSubmission = new FormSubmission();
        $formSubmission->name = $request->input('name');
        $formSubmission->email = $request->input('email');
        $formSubmission->password = bcrypt($request->input('password'));
        $formSubmission->details = $details;
        $formSubmission->save();
        return redirect()->back()->with('success', 'Form submitted successfully!');
    }
    public function view_job(){
        $data = FormSubmission::find(5);
        return view('jobview',compact('data'));
    }
    public function all_view(){
        $data=FormSubmission::get();
        return view('allview',compact('data'));
    }
}
